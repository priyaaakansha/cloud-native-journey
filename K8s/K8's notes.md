# K8’s notes

Kubernetes workshop

- Introduction to k8s

**What is k8s?**

K8’s is an open-source system for deploying, scaling and managing containerized applications.

- CNCF graduated project
- designed for massive scale
- run anywhere

**Why k8s**
- few containers are fine but what about scale?
- How do monitor them ?

- What about monitoring them?
- What about running pods on multiple pods?
- What about flexibility?
- Autoscaling?
- Scheduling?
- self-healing capabilities

**Kubernetes Architecture**

Containers technology - docker 

Why do we need containers ?

- compatibility/dependency
- long setup time
- different dev/test/prod environments

**What can containers do ?**

- containerize applications
- run each service with its own dependencies in separate containers

The main purpose of dockers is to containerise applications and to ship them and run them.

**Containers vs virtual machines** 

- Vms has their own os unlike containers —> therefore higher utilisations of underlying resources as there are multiple virtual operating systems
- Vms consume higher disk space
- booting up takes time
- Vm’s have complete isolation from each other, since mediums dont rely on the underlying operating system or Kernel, you can have diff types of OS such as Linux based or windows based on the same hypervisor whereas it is not possible on a single docker host.

**Containers vs Image**

Image is a template used to create one or more containers.

And containers are running instances of images that are isolated and have their own environments and set of processes.

**Container Orchestration**

The whole process of automatically deploying and managing containers is known as container orchestration.

K8s is a container orchestration technology.

**K8s Architecture**

1. Node - it is a machine physical or virtual on which K8s is installed. Here the containers will be launched by the k8s.
2. Cluster - A cluster is a set of nodes grouped together. Multiple nodes helps in sharing load as well.
3. Master Node  - so how are the nodes monitored ? - when a nodes fail how do you move the workload of the field node to another worker node? 
The master watches over the nodes in the cluster and is responsible for the actual orchestration of containers on the worker nodes.
4. API server - acts as the front end of the k8s. The users, management devices, cli, all talk to the api server to interact with the k8s cluster. 
5. etcd key store - is a distributed reliable key value store used by k8s to store all date used to manage the cluster. so when you have mutiple nodes and mutiple masters in your cluster, etcd stores all that information in a distributed manner. It is responsible for implementing locks within the cluster to ensure that there are no conflicts between the masters. 
6. Schedulers - is responsible for distributing work or containers. It looks for newly created containers and assign them to nodes.
7. Controllers - are the brain behind orchestration. They are responsible for noticing and responding when nodes, containers, or end points goes down. They make the decision to bring up new containers in such cases.
8. Container runtime - is the underlying software that is used to run containers. eg docker.
9. Kubelet - is the agent that runs on each node in the cluster - making sure that the containers are running on the nodes as expected.

**kubectl** 

few commands 

- kubectl run hello-minikube
- kubectl cluster-info
- kubectl get nodes

**Pods**

- k8s does not deploy containers directly on a worker node.
- the containers are encapsulated into a k8s objects called pods
- A pod is a single instance of an application - smalled object that you can create in k8s
- Pods usually have one to one relationship with containers - but it can have muti container as well - take the example of helper container.

**How to deploy and run a pod ?**

—> kubectl run nginx —image=nginx

—> kubectl get pods

—> kubectl describe pod nginx

—> kubectl get pods -o wide  - tells additional info that where the pod is running and the IP address of the pod as well.

**What is YAML ?**

- a yaml file is used to represent data, of a sample data in three different formats.

**Pods with YAML**

4 top level fields

- apiVersion
- kind
- metadata
- spec

command - kubectl create -f pod-definition.yaml

**Replication controllers and ReplicaSets**

Replication controller benefit 

- high availability
- load balancing and scaling

ReplicaSet is the newer technology.

The major difference is the **selector** 

- a user input is required - matchLabels selectors simply matches the labels specified under to it to the labels on the pods.

**To scale the replicas**

- kubectl replace -f replicaset.yml
- kubectl scale —replicas=6 -f replicaset.yml
- kubectl scale —replicas=6 replicaset myapp-replicaset

**Deployments**

it provides us with the capability to upgrade the underlying instances, seamlessly using rolling updates, and pause and resume changes as required.

**Kubernetes networking 101**

- IP address is assigned to a POD
- Cluster networking conditions - all containers/PODs can communicate to one another without NAT
- All nodes can communicate with all containers and vice-versa without NAT.

You can use cilium to do so.

**Services** 

In Kubernetes, there are three commonly used Service types: ClusterIP, NodePort, and LoadBalancer.

**NodePort**

Kubernetes services or deployments are like object, just like pods, replica sets. One of its use case is to listen to a port on the node and forward request on that port to a port on the pod running the web application. This type of service is called nodePort service.

Type of port 

- target port
- port
- node port

**ClusterIP**

In Kubernetes, the **ClusterIP Service is used for Pod-to-Pod communication within the same cluster**. ****This means that a client running outside of the cluster, such as a user accessing an application over the internet, cannot directly access a ClusterIP Service.

**A LoadBalancer Service is another way you can expose your applications to external clients.** However, it only works if you're using Kubernetes on a cloud platform that supports this Service type.

The load balancer will have its own unique, publicly accessible IP address that clients can use to connect to your application.